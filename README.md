# MYJ2C

#### 介绍
MYJ2C是一款Java混淆工具，将编译好的Class文件转换为C语言代码，然后交叉编译（您不用自己配置编译环境，MYJ2C自动完成）可以生成Windows，Linux，Mac系统X86，ARM平台的动态链接库文件后，通过Java Native Interface 重新链接到原始程序。在此过程结束时，包含原始方法的.class文件的字节码中不会保留原始方法的信息。编译后的class文件对Java逆向工程工具完全隐藏。使用MYJ2C不需要任何Java JNI或C代码的知识。MYJ2C支持所有Java语言特性，可以在现有的代码库中使用。

编译前
```
public class App {
	public static void main(String args[]) {
		System.out.println("Hello, world!");
	}
}
```
编译后

```
public class App {
	public static native void main(String args[]);
}
```

#### 安装教程

1.  下载最新发布版本 [点此下载](https://gitee.com/myj2c/myj2c/releases)
2.   **放到不含中文路径的目录中** 
3.  使用JDK8以上版本

#### 效果展示

| 名称 | 原始代码 | myj2c混淆 | 说明 |
|----|------|---------|----|
| SnakeGame.jar  | [SnakeGame 右键另存为](https://gitee.com/myj2c/myj2c/raw/master/SnakeGame.jar) | [MYJ2C混淆 右键另存为](https://gitee.com/myj2c/myj2c/raw/master/SnakeGame-myj2c.jar)| 一款贪吃蛇游戏 [GitHub](https://github.com/hexadeciman/Snake)|


#### 使用说明

 _运行默认配置_
1.启动UI模式
```bash
java -jar myj2c.jar
```
![输入图片说明](UI.png)

选择输入输出文件

![输入图片说明](config.png)
配置全局混淆，此处需要混淆类，方法，属性是所有混淆生效，返回true是需要混淆，false不混淆
需要混淆的类中node属性支持node.superName，node.name，node.desc 可以使用正则表达式，字符串比较方法contains等
![输入图片说明](myj2cobf.png)
这里配置myj2c混淆规则
![输入图片说明](myj2c.png)
这里可以生成配置文件，可以导入导出配置

2.  在命令行切换到myj2c.jar所在目录，运行java -jar myj2c.jar 待编译jar路径 输出目录或文件路径
```bash
java -jar myj2c.jar D:\MYJ2C-DEMO\SnakeGame.jar D:\MYJ2C-DEMO\SnakeGame -c config.mycfg
```
3.  自定义配置

```
{
  "输入:": "C:\\MYJ2C-DEMO\\SnakeGame.jar",
  "输出:": "C:\\MYJ2C-DEMO\\SnakeGame-out.jar",
  "依赖库": [],
  "打乱类成员顺序": {
    "启用": false,
    "需要混淆的类": "//配置混淆类,默认全局配置类\nfunction isObfuscatorEnabledForClass(node) {\n    //print(node.name+node.desc);\n    return true;\n}\n"
  },
  "流程混淆": {
    "启用": true,
    "混淆数字比较": true,
    "混淆GOTO": true,
    "混淆If": true,
    "混淆Concat": false,
    "混淆Switch": false,
    "需要混淆的类": "//配置混淆类,默认全局配置类\nfunction isObfuscatorEnabledForClass(node) {\n    //print(node.name+node.desc);\n    return true;\n}\n",
    "需要混淆的方法": "//配置混淆方法,默认全局配置方法\nfunction isObfuscatorEnabledForMethod(owner,methodNode) {\n    //print(owner+\u0027,\u0027+methodNode.name+\u0027\u0027+methodNode.desc);\n    return true;\n}\n"
  },
  "类文件格式": {
    "启用": false,
    "无效签名": false,
    "需要混淆的类": "//配置混淆类,默认全局配置类\nfunction isObfuscatorEnabledForClass(node) {\n    //print(node.name+node.desc);\n    return true;\n}\n"
  },
  "字符串混淆": {
    "启用": true,
    "需要混淆的类": "//配置混淆类,默认全局配置类\nfunction isObfuscatorEnabledForClass(node) {\n    //print(node.name+node.desc);\n    return true;\n}\n",
    "需要混淆的方法": "//配置混淆方法,默认全局配置方法\nfunction isObfuscatorEnabledForMethod(owner,methodNode) {\n    //print(owner+\u0027,\u0027+methodNode.name+\u0027\u0027+methodNode.desc);\n    return true;\n}\n"
  },
  "名称混淆": {
    "启用": false,
    "需要混淆的类": "//配置混淆类,默认全局配置类\nfunction isObfuscatorEnabledForClass(node) {\n    //print(node.name+node.desc);\n    return true;\n}\n",
    "需要混淆的方法": "//配置混淆方法,默认全局配置方法\nfunction isObfuscatorEnabledForMethod(owner,methodNode) {\n    //print(owner+\u0027,\u0027+methodNode.name+\u0027\u0027+methodNode.desc);\n    return true;\n}\n",
    "需要混淆的属性": "function isObfuscatorEnabledForField(owner,fieldNode) {\n    //print(owner+\u0027,\u0027+fieldNode.name+\u0027\u0027+fieldNode.desc);\n    return true;\n}\n",
    "混淆包名": false,
    "新包名": "",
    "更新资源文件中类名": false,
    "需要更新文件列表": ""
  },
  "动态调用混淆": {
    "启用": true,
    "需要混淆的类": "//配置混淆类,默认全局配置类\nfunction isObfuscatorEnabledForClass(node) {\n    //print(node.name+node.desc);\n    return true;\n}\n",
    "需要混淆的方法": "//配置混淆方法,默认全局配置方法\nfunction isObfuscatorEnabledForMethod(owner,methodNode) {\n    //print(owner+\u0027,\u0027+methodNode.name+\u0027\u0027+methodNode.desc);\n    return true;\n}\n"
  },
  "数字混淆": {
    "启用": true,
    "使用数组": true,
    "混淆零": true,
    "使用移位运算": true,
    "使用逻辑运算": true,
    "使用多次混淆": false,
    "需要混淆的类": "//配置混淆类,默认全局配置类\nfunction isObfuscatorEnabledForClass(node) {\n    //print(node.name+node.desc);\n    return true;\n}\n",
    "需要混淆的方法": "//配置混淆方法,默认全局配置方法\nfunction isObfuscatorEnabledForMethod(owner,methodNode) {\n    //print(owner+\u0027,\u0027+methodNode.name+\u0027\u0027+methodNode.desc);\n    return true;\n}\n"
  },
  "隐藏类成员": {
    "启用": false,
    "需要混淆的类": "//配置混淆类,默认全局配置类\nfunction isObfuscatorEnabledForClass(node) {\n    //print(node.name+node.desc);\n    return true;\n}\n",
    "需要混淆的方法": "//配置混淆方法,默认全局配置方法\nfunction isObfuscatorEnabledForMethod(owner,methodNode) {\n    //print(owner+\u0027,\u0027+methodNode.name+\u0027\u0027+methodNode.desc);\n    return true;\n}\n",
    "需要混淆的属性": "function isObfuscatorEnabledForField(owner,fieldNode) {\n    //print(owner+\u0027,\u0027+fieldNode.name+\u0027\u0027+fieldNode.desc);\n    return true;\n}\n"
  },
  "去除行号": {
    "启用": false,
    "变量重命名": false,
    "移除行号": false,
    "增加变量": false,
    "新的源文件名": "",
    "需要混淆的类": "//配置混淆类,默认全局配置类\nfunction isObfuscatorEnabledForClass(node) {\n    //print(node.name+node.desc);\n    return true;\n}\n"
  },
  "删除内部类": {
    "启用": false,
    "重新映射": false,
    "删除元数据": false,
    "需要混淆的类": "//配置混淆类,默认全局配置类\nfunction isObfuscatorEnabledForClass(node) {\n    //print(node.name+node.desc);\n    return true;\n}\n"
  },
  "全局配置": {
    "iii字符": true,
    "Unicode统一字符编码": true,
    "Java关键字": true,
    "abc字母": true,
    "增加\\n \\r": true,
    "自定义词典": false,
    "类名字典": "",
    "名称字典": "",
    "需要混淆的类": "//配置混淆类,全局配置\nfunction isObfuscatorEnabledForClass(node) {\n    //print(node.superName+node.name+node.desc);\n    //不混淆Thread子类\n    if (node.superName!\u003dnull\u0026\u0026node.superName.contains(\"Thread\")){\n       return false;\n    }\n    //不混淆依赖库\n    if(node.name.match(\"^cn\\/myj2c\\/library\\/.*?\")){\n       return false;\n    }\n    //不混淆以下包\n    if(node.name.match(\"^ch\\/qos\\/.*?\") || node.name.match(\"^com\\/.*?\") || node.name.match(\"^org\\/.*?\")){\n       return false;\n    }\n    return true;\n}\n",
    "需要混淆的方法": "//配置混淆方法,全局配置\nfunction isObfuscatorEnabledForMethod(owner,methodNode) {\n    //print(owner+\u0027,\u0027+methodNode.name+\u0027\u0027+methodNode.desc);\n    //if(owner.contains(\"cn/myj2c\")){\n    //   return false;\n    //}\n    //不混淆包含test的方法\n    if(methodNode.name.contains(\"test\")){\n       return false;\n    }\n    //不混淆包含test的方法\n    if(methodNode.desc\u003d\u003d\"()V\"){\n       return false;\n    }\n    return true;\n}\n",
    "需要混淆的属性": "//配置混淆属性,全局配置\nfunction isObfuscatorEnabledForField(owner,fieldNode) {\n    //print(owner+\u0027,\u0027+fieldNode.name+\u0027\u0027+fieldNode.desc);\n    //不混淆包含test的属性\n    if(fieldNode.name.contains(\"test\")){\n       return false;\n    }\n    //不混淆String型属性\n    if(fieldNode.desc \u003d\u003d\"Ljava/lang/String;\"){\n       return false;\n    }\n    return true;\n}\n"
  },
  "MYJ2C混淆": {
    "启用": true,
    "请选择MYJ2C编译平台": "",
    "单独打包": false,
    "WINDOWS_X86_64": true,
    "WINDOWS_AARCH64": true,
    "MACOS_X86_64": true,
    "MACOS_AARCH64": true,
    "LINUX_X86_64": true,
    "LINUX_AARCH64": true,
    "开启过期": false,
    "过期日期(yyyy-MM-dd)": "2023-11-26",
    "调试模式": false,
    "保留方法代码": false,
    "字符串混淆": true,
    "混淆java加密字符串数据": true,
    "混淆java字符串加密方法": true,
    "混淆java动态调用数据": true,
    "混淆java动态调用方法": true,
    "混淆java数字混淆方法": true,
    "快速编译": false,
    "需要混淆的类": "//配置混淆类,默认全局配置类\nfunction isObfuscatorEnabledForClass(node) {\n    //print(node.name+node.desc);\n    return true;\n}\n",
    "需要混淆的方法": "//配置混淆方法,默认全局配置方法\nfunction isObfuscatorEnabledForMethod(owner,methodNode) {\n    //print(owner+\u0027,\u0027+methodNode.name+\u0027\u0027+methodNode.desc);\n    //请在这里增加要编译的方法规则，否则将不会编译任何方法\n    return true;\n}\n"
  }
}
```

目前可以编译WINDOWS，LINUX，MACOS系统X86_64和ARM架构动态链接库：

```bash
    WINDOWS_X86_64
    WINDOWS_AARCH64
    MACOS_X86_64
    MACOS_AARCH64
    LINUX_X86_64
    LINUX_AARCH64
```


#### 注意事项
配置混淆规则
混淆类
```
//判断是否混淆,会把当前类输入获取返回值true或false 当返回true进行混淆，返回false不混淆
//node 为当前类属性
function isObfuscatorEnabledForClass(node) {
    //支持属性node.superName node.name node.desc 
    //不混淆Thread子类
    if (node.superName!=null&&node.superName.contains("Thread")){
       return false;
    }
    //不混淆依赖库
    if(node.name.match("^cn\/myj2c\/library\/.*?")){
       return false;
    }
   //混淆cn.myj2c包所有类
    if(owner.contains("cn/myj2c")){
       return true;
    }
    //混淆cn.myj2c.Main类
    if(owner.contains("cn/myj2c/Main")){
       return true;
    }
    //不混淆以下包
    if(node.name.match("^ch\/qos\/.*?") || node.name.match("^com\/.*?") || node.name.match("^org\/.*?")){
       return false;
    }
    return true;
}

```
混淆方法

```
//判断是否混淆类的方法，会把当前类名和方法输入获取返回值true或false 当返回true进行混淆，返回false不混淆
//owner为当前类名 methodNode为方法属性
function isObfuscatorEnabledForMethod(owner,methodNode) {
    //支持属性methodNode.name methodNode.desc 
    //if(owner.contains("cn/myj2c")){
    //   return false;
    //}
    //不混淆包含test的方法
    if(methodNode.name.contains("test")){
       return false;
    }
    //不混淆包含test的方法
    if(methodNode.desc=="()V"){
       return false;
    }
    return true;
}
```
混淆属性

```
//判断是否混淆类的属性，会把当前类名和属性输入获取返回值true或false 当返回true进行混淆，返回false不混淆
function isObfuscatorEnabledForField(owner,fieldNode) {
    //支持属性 fieldNode.name fieldNode.desc 
    //不混淆cn.myj2c包里面的所有属性
    //if(owner.contains("cn/myj2c")){
    //   return false;
    //}
    //不混淆包含test的属性
    if(fieldNode.name.contains("test")){
       return false;
    }
    //不混淆String型属性
    if(fieldNode.desc =="Ljava/lang/String;"){
       return false;
    }
    return true;
}

```

  

>    较旧的Java编译器可能会发出JSR和RET指令，这是Java 7字节码或更新版本中不允许的弃用指令。MYJ2C仅支持Java8字节码及以上版本，因此无法处理JSR/RET指令。如果您的应用程序中有包含Java 6类文件的较旧库，则应将它们排除。 
>     Java 11中允许一个称为ConstantDynamic的新特性，它允许在运行时通过引导方法动态初始化常量池条目。包含ConstantDynamic条目的类文件目前与MYJ2C不兼容。应将它们排除。
> 
>     JNI接口的限制限制了任何转换方法的性能。特别是，与Java相比，方法调用、字段访问和数组操作速度较慢。在一些要求性能的方法应将他们排除
>     在JNI代码中，算术、强制转换、控制流和局部变量访问仍然非常快（甚至可以通过C编译器进行优化）
> 
#### 编写安全的代码

MYJ2C是一个强大的混淆器，但其有效性可能会受到其翻译的代码的限制。考虑以下几点：

不安全的写法

```bash
public class App {
	public static void main(String[] args) {
		if(!checkLicence()) {
			System.err.println("Invalid licence");
			return;
		}
		initApp();
		runApp();
	}

	private static native boolean checkLicence(); // Protected by MYJ2C

	private static native void initApp(); // Protected by MYJ2C

	private static native void runApp(); // Protected by MYJ2C
}
```

在此示例中，即使checkLicence代码受MYJ2C保护，攻击者也很容易修改主方法，直接返回 true 达到破解目的。

```bash
public class App {
	public static void main(String[] args) {
		if(!checkLicence()) {
			System.err.println("Invalid licence");
			return;
		}
		initApp();
		runApp();
	}

	private static boolean checkLicence() {
	    return true; //这样就可以达到破解目的，绕过正常授权验证方法
	}

	private static native void initApp(); // Protected by MYJ2C

	private static native void runApp(); // Protected by MYJ2C
}
```

比较好的写法

```bash
public class App {
	public static void main(String[] args) {
		checkLicenceAndInitApp();
		runApp();
	}

	private static native void checkLicenceAndInitApp(); // Protected by MYJ2C
}
```


在这里，攻击者即使修改CheckLicensandInApp的方法，跳过授权部分但是也不知道里面要执行哪些初始化功能，修改之后应用程序将无法正常运行（因为它将无法初始化）。

推荐使用MYJ2C保护应用程序的初始化代码，因为它只在运行的时候执行一次，初始化方法不会反复被执行不用担心有性能问题。 


#### 授权信息

1.免费版 只能编译一个类中的一个方法，可以永久免费使用

2.试用版 免费使用，编译后的jar包可以免费使用一周（7天），之后程序将无法使用

3.个人版 可按月按年付费，控制台会打印myj2c的编译信息，控制台信息可定制，不能去除，同时有编译数量限制

4.专业版 可按月按年付费，控制台无任何信息输出，没有编译数量限制

#### 常见问题

1.  无法编译动态链接库

    答：可能是路径中有中文或特殊字符，导致zig无法编译，修改路径放到非中文或特殊字符的路径下执行编译

2.  需要编译全平台吗

    答：默认配置是编译windows，linux，mac系统支持64位和arm平台，您可以根据自己的需求配置编译的平台

3.  是否开启字符串混淆，流程混淆

    答：开启混淆字符串、流程混淆功能，编译时间会延长，编译的动态链接库文件体积会增加，但是安全性也会提升，请根据实际情况选择是否开启

4.  MYJ2C会对我的应用程序的性能产生重大影响吗？

    答：许多代码保护工具必须在性能和安全性之间进行权衡。我们建议您仅在敏感代码或性能不重要的代码上使用它。

5.  MYJ2C是否支持lambdas/streams/exceptions/threads/locks/。。。？

    答：MYJ2C对编译的Java字节码进行操作，支持Java 8或更高版本的JVM编译的任何字节码。MYJ2C支持Java中的所有语言特性，并且还支持在JVM上运行的其他编程语言，如                kotlin。

6.  与自己编写JNI方法相比，MYJ2C有哪些优势？

    答：编写使用Java本机接口的代码非常困难，而且这种代码通常更难调试。MYJ2C允许您编写（和测试！）Java代码，请使用Java中的所有代码。此外：
        MYJ2C可以翻译Java混淆器的输出，如Zelix Klassmaster或Stringer。
        MYJ2C可以在Java API中转换在C中没有直接等价的东西，如lambdas、方法引用和流。
        使用MYJ2C，您不需要知道如何使用JNI或C，也不需要编写在运行时将本机库链接到应用程序的代码（MYJ2C自动注入）。
        MYJ2C可以翻译现有的Java代码——您不需要浪费时间重写已经完成的应用程序部分。

7.  在使用MYJ2C混淆之前，我可以对它们应用额外的混淆处理吗？

    答：当然，这是可能的，尽管我们不能保证任何代码混淆工具的兼容性。此外，如果在运行MYJ2C之前已经使用了Java混淆工具，则进一步混淆文件可能是不必要的。
 
8.  在运行MYJ2C之后，我可以对输出JAR文件应用额外的混淆处理吗？

    答：对已用MYJ2C混淆的任何方法/字段/类使用名称混淆将导致运行时由于链接不满足而崩溃。您可以自由使用字符串混淆、引用混淆、资源加密等。

9.  运行报LOAD ERROR:Illegal key size异常
 
    答：替换jdk/jre/lib/security目录中的local_policy.jar和US_export_policy.jar文件，文件在jce_policy-8.zip [右键另存为](https://gitee.com/myj2c/myj2c/raw/master/jce_policy-8.zip)中，下载解压即可获取到以上文件

10.  联系方式

    答：请加QQ群：197453088

